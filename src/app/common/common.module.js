import angular from 'angular';

const commonModule = angular
  .module('app.common', [])
  .name;

export default commonModule;

import angular from 'angular';
import FizzBuzzModule from './fizz-buzz/fizz-buzz.module';

const componentsModule = angular
  .module('app.components', [
    FizzBuzzModule,
  ])
  .name;

export default componentsModule;

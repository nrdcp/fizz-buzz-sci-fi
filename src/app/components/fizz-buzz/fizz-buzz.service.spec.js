import FizzBuzzService from './fizz-buzz.service';

let service;

describe('FizzBuzzService', () => {
  beforeEach(() => {
    service = new FizzBuzzService();
  });

  describe('run', () => {
    let errorMsg = {error: 'INVALID_INPUT'};

    it('returns an error when no input is provided', () => {
      return service.run()
        .then((result) => {
          throw new Error();
        })
        .catch((result) => {
          expect(result).to.deep.equal(errorMsg);
        })
    });

    it('returns an error when an a negative input value is provided', () => {
      return service.run(-1)
        .then((result) => {
          throw new Error();
        })
        .catch((result) => {
          expect(result).to.deep.equal(errorMsg);
        })
    });

    it('returns an error when 0 is provided', () => {
      return service.run(0)
        .then((result) => {
          throw new Error();
        })
        .catch((result) => {
          expect(result).to.deep.equal(errorMsg);
        })
    });

    it('returns the result when a positive integer is provided', () => {
      return service.run(14)
        .then((result) => {
          expect(result.fizz).to.equal(4);
          expect(result.buzz).to.equal(2);
          expect(result.fizzBuzz).to.equal(0);
        })
    });

    it('returns the result when a positive integer is provided', () => {
      return service.run(14)
        .then((result) => {
          expect(result.fizz).to.equal(4);
          expect(result.buzz).to.equal(2);
          expect(result.fizzBuzz).to.equal(0);
        })
    });

    it('returns an error when an invalid string is provided', () => {
      return service.run('yo')
        .then((result) => {
          throw new Error();
        })
        .catch((result) => {
          expect(result).to.deep.equal(errorMsg);
        })
    });

    it('returns the result when a valid string value is provided', () => {
      return service.run('15')
        .then((result) => {
          expect(result.fizz).to.equal(5);
          expect(result.buzz).to.equal(3);
          expect(result.fizzBuzz).to.equal(1);
        })
    });
  });
});

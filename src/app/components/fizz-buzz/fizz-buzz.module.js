import angular from 'angular';
import uiRouter from 'angular-ui-router';
import HomeComponent from './fizz-buzz.component';
import FizzBuzzService from './fizz-buzz.service';

const HomeModule = angular
  .module('fizzBuzz', [
    uiRouter,
  ])
  .config(($stateProvider, $urlRouterProvider) => {
    'ngInject';

    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('fizzBuzz', {
        url: '/',
        component: 'fizzBuzz',
      });
  })
  .component('fizzBuzz', HomeComponent)
  .service('FizzBuzzService', FizzBuzzService)
  .name;

export default HomeModule;


class FizzBuzzController {
  constructor($scope, FizzBuzzService) {
    'ngInject';

    this.$scope = $scope;
    this.FizzBuzzService = FizzBuzzService;
  }

  $onInit() {
    this.numIterations = '';
    this.results = null;
    this.states = {
      error: false,
    };

    // this.numIterations = 1000;
    // this.getResults();
  }

  onSubmit() {
    if (!!this.numIterations < 1) {
      this.states.error = true;
      return;
    }

    this.getResults();
  }

  getResults() {
    this.states.error = false;

    this.FizzBuzzService.run(this.numIterations)
      .then((results) => {
        this.results = results;
        this.$scope.$apply();
      })
      .catch((errorDetails) => {
        this.states.error = true;
        this.results = null;
        this.$scope.$apply();
        console.log(errorDetails);
      });
  }
}

export default FizzBuzzController;

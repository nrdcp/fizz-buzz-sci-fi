
class FizzBuzzService {
  run(value) {
    let results = {
      fizz: 0,
      buzz: 0,
      fizzBuzz: 0,
    };

    let numIterations = parseInt(value, 10);

    if (isNaN(numIterations) || numIterations < 1) {
      return Promise.reject({
        error: 'INVALID_INPUT',
      });
    }

    for (let i = 1; i < numIterations + 1; i += 1) {
      if (i % 3 === 0) {
        results.fizz += 1;
      }
      if (i % 5 === 0) {
        results.buzz += 1;
      }
      if (i % 3 === 0 && i % 5 === 0) {
        results.fizzBuzz += 1;
      }
    }

    return Promise.resolve(results);
  }
}

export default FizzBuzzService;


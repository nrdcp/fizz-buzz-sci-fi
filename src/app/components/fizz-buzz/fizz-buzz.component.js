import template from './fizz-buzz.html';
import controller from './fizz-buzz.controller';
import './fizz-buzz.scss';

const FizzBuzzComponent = {
  bindings: {},
  template,
  controller,
};

export default FizzBuzzComponent;

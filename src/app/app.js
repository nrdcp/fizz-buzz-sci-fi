
import './styles/main.scss';
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import CommonModule from './common/common.module';
import ComponentsModule from './components/components.module';
import AppComponent from './app.component';
import './app.scss';

angular
  .module('app', [
    uiRouter,
    CommonModule,
    ComponentsModule,
  ])
  .config(($locationProvider) => {
    'ngInject';

    // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
    // #how-to-configure-your-server-to-work-with-html5mode
    $locationProvider.html5Mode(true).hashPrefix('!');
  })
  .component('app', AppComponent);

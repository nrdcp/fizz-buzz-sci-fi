# Fizz Buzz Sci-Fi APP

## Getting Started
### Dependencies
Tools needed to run this app:
* `node` and `npm`

## Demo
Visit [http://www.nrdcp.net/fizz-buzz-sci-fi](http://www.nrdcp.net/fizz-buzz-sci-fi) to see a live project demo.

### Installing
* `npm install -g gulp karma karma-cli webpack` install global cli dependencies
* `npm install` to install dependencies

#### Gulp Tasks
Here's a list of available tasks:
* `serve`
  * starts a dev server via `webpack-dev-server`, serving the `src` folder.
* `build`
  * runs Webpack, which will transpile, concatenate, and compress (collectively, "bundle") all assets and modules into `dist/bundle.js`. It also prepares `index.html` to be used as application entry point, links assets and created dist version of our application.
